
var ctx = document.getElementById('barChart').getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY', 'JUNE'],
    datasets: [
    {
      label: 'Borrowed Books',
      data: [7, 10, 9, 12, 13, 6],
      backgroundColor: 'rgba(61, 124, 191)',
    },
    {
      label: 'Returned Books',
      data: [4, 7, 10, 11, 8, 8],
      backgroundColor: 'rgba(181, 63, 196)',
    }
  ],
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});

var pie = document.getElementById('pieChart').getContext('2d');
var pieChart = new Chart(pie, {
  type: 'doughnut',
  data: {
    datasets: [{
        data: [10, 20, 30, 20, 10, 10],
        backgroundColor: [
          'rgb(209, 105, 124)',
          'rgba(116, 200, 204)',
          'rgba(182, 109, 191)',
          'rgba(229, 161, 35, .8)',
	  'rgba(216, 224, 117)',
	  'rgba(74, 204, 34, .8)',
	
        ],
    }],

    labels: [
        'Sci Fi',
        'Drama',
        'Fairytale',
        'Horror',
	'Mystery',
	'Comic book',
    ]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});


